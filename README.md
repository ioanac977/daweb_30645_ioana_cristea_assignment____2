# DESCRIPTION 
Improve the web application developed in Assignment 1 using the specifications below.
# REQUIREMENTS
1. Implement the Language Selection functionality (RO and EN). Save user preference for the language in which the information is displayed using HTML 5 Web Storage.
2. Use JS to dynamically retrieve the corresponding information from the News menu in XML files
3. Implement an email submission form on the Contact page
# REMARKS 
- Use HTML 5, CSS and JS languages
- For requirement no. 3 for implementation of Server-side components use Flask
# CONFIGURATIONS 
- install NodeJs 
- install pip
- install Python 
- install Flask
- install WebStorm
- install PyCharm
# RUN
- download repository
## FRONTEND
- open frontend project using WebStorm
- go to src directory 
- run on terminal "npm install"
- run on terminal "npm start"
## BACKEND 
- open frontend project using PyCharm
- configure email properties
```
SENDER = '{defaultSenderEmail}@{emailExtension}.com'
SENDERNAME = '{defaultSenderName}'
USERNAME_SMTP = "{yourEmail}@{yourExtentionEmail}.com"
PASSWORD_SMTP = "{yourGeneratedPasswordForEmailApplications"
HOST = "{HostAdress}"  //for SMTP , usually smtp.gmail.com
PORT = 587
```
- run on terminal "python run.py run"
# NAVIGATION 
- the frontend application will start on http://localhost:3000/
- the backend application will start on  http://127.0.0.1:5000/

