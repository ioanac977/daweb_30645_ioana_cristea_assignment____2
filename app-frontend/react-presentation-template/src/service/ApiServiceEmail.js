import axios from 'axios';

const EMAIL_API_BASE_URL = 'http://127.0.0.1:5000/';

class ApiServiceEmail {


    addEmail(user) {
        return axios.post(""+EMAIL_API_BASE_URL, user);
    }

}

export default new ApiServiceEmail();
