##DEMO

![](demo_email_sending.gif)
# Flask Mail Sender

Sends mail with data from the post method.


## Install 
Open requirements.txt and install dependencies from the file. 
In this project, requirements.txt contain flask. 
For installation run:
```python3
pip install flask
```

For the next step, open run.py and edit the information by yourself.

```
# Email
SENDER = '{defaultSenderEmail}@{emailExtension}.com'
SENDERNAME = '{defaultSenderName}'
USERNAME_SMTP = "{yourEmail}@{yourExtentionEmail}.com"
PASSWORD_SMTP = "{yourGeneratedPasswordForEmailApplications"
HOST = "{HostAdress}"  //for SMTP , usually smtp.gmail.com
PORT = 587
```
#Run  the application

`python run.py run`

## How it works?

In Postman : 
```bash
curl -X POST \
  http://127.0.0.1:5000/ \
  -H 'content-type: application/json' \
  -d '{
	"recipient": "tolgahanuzun2@gmail.com",
	"subject": "Flask Mail Sender",
	"body": "<br><br> <b>Mail test</b>"
    }'
```
